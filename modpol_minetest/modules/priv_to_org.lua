--- Set privilege to org members
-- Allows initiator to grant a priv they have to all members of an org
-- @module priv_to_org

local priv_to_org = {
    name = "Set privilege to org members",
    slug = "priv_to_org",
    desc = "Allows initiator to grant a priv they have to all members of an org"
}

priv_to_org.data = {
}

priv_to_org.config = {
}

--- Initiate function
-- @function priv_to_org:initiate
-- @param result
function priv_to_org:initiate(result) 
   local player_privs = minetest.get_player_privs(self.initiator)
   -- construct table for display
   local player_privs_table = {}
   for k,v in pairs(player_privs) do
      if player_privs[k] then
         table.insert(player_privs_table,k)
      end
   end
   modpol.interactions.dropdown_query(
      self.initiator,
      "Which privilege do you want to share with members of "..self.org.name.."?",
      player_privs_table,
      function(input)
         modpol.interactions.org_dashboard(self.initiator,self.org.id)
         for i,member in ipairs(self.org.members) do
            local member_privs = minetest.get_player_privs(member)
            member_privs[input] = true
            minetest.set_player_privs(member, member_privs)
         end
         local message = self.initiator .. " added " .. input ..
         " privilege to all members of " .. self.org.name
         modpol.interactions.message_org(self.initiator, self.org.id, message)
   end)
    -- call result function 
    if result then result() end
end

modpol.modules.priv_to_org = priv_to_org
