--- Script for loading Minetest files

-- ===================================================================
-- Minetest Specific Overrides of modpol functions
-- ===================================================================


local localdir = minetest.get_modpath("modpol") .. "/modpol_minetest"

--overrides
dofile (localdir .. "/overrides/interactions.lua")
dofile (localdir .. "/overrides/users.lua")

-- ===================================================================
-- Minetest Chatcommands
-- ===================================================================
dofile (localdir .. "/chatcommands.lua")


-- ===================================================================
-- Minetest-specific modules
-- ===================================================================

dofile (localdir .. "/modules/priv_to_org.lua")

-- ===================================================================
-- Minetest-specific code
-- ===================================================================

--add members to the instance, if they are not already there.
minetest.register_on_joinplayer(function(player)
    local p_name = player:get_player_name()
    modpol.instance:add_member(p_name)
end)
