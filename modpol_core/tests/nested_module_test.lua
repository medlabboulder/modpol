dofile('../modpol.lua');

modpol.orgs.reset()

test_org = modpol.instance:add_org('test_org', 'luke')
test_org:add_member('nathan')

print(table.concat(test_org:list_members(), ", "))

test_org:call_module(
    "join_org_consent",
    "paul"
)