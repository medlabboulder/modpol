--- change_policy
-- @module change_policy

local change_policy = {
    name = "Change policy",
    slug = "change_policy",
    desc = "Change a policy in a module",
    hide = false;
}

change_policy.data = {
   result = false
}

change_policy.config = {
   approval_module = false
}

--- Change modules initiate
-- @function change_policy:initiate
-- @param result Callback if this module is embedded in other modules
function change_policy:initiate(result)
   -- prepare module options
   local available_modules = {}
   for k,org_mod in pairs(modpol.modules) do
      if self.org.policies[k] then
         available_modules[org_mod.slug] = modpol.util.copy_table(org_mod)
   end end
   local modules_list = {}
   local modules_count = 0
   for k,v in pairs(available_modules) do
      table.insert(modules_list,v.slug)
      modules_count = modules_count + 1
   end
   -- abort if no modules to remove
   if modules_count == 0 then
      modpol.interactions.message(
         self.initiator, "Org has no modules")
      modpol.interactions.org_dashboard(
         self.initiator, self.org.id)
      if self.data.result then self.data.result() end
      self.org:delete_process(self.id)
      return
   end
   table.sort(modules_list)
   -- now ask which to remove
   modpol.interactions.dropdown_query(
      self.initiator, "Choose a module to change policies for:",
      modules_list,
      function(mod_choice)
         local this_module = modpol.modules[mod_choice]
         local module_policies = this_module.config
         local policy_list = {}
         for k,v in pairs(module_policies) do
            table.insert(policy_list, k)
         end
         if #policy_list == 0 then
            -- No policies; abort
            modpol.interactions.message(
               self.initiator, "Module has no policy options")
            modpol.interactions.org_dashboard(
               self.initiator, self.org.id)
            if self.data.result then self.data.result() end
            self.org:delete_process(self.id)
            return
         end
         modpol.interactions.dropdown_query(
            self.initiator, "Choose a policy to change:",
            policy_list,
            function(policy_choice)
               local readable_value =
                  tostring(modpol.modules[mod_choice][policy_choice])
               if readable_value == "nil" then
                  readable_value = "none"
               end                     
               modpol.interactions.text_query(
                  self.initiator,
                  "Current " .. policy_choice .. " value: "
                  .. readable_value
                  .. "\nChange value to (or leave blank):",
                  function(policy_input)
                     if policy_input == "" then
                        policy_input = false
                     end
                     self:approve_change(
                        mod_choice,
                        policy_choice,
                        policy_input)
                  end
               )
            end
         )
      end
   )
end

--- Propose a change.
-- Type "add" or "remove"
-- @function change_policy:approve_change
-- @param module (string) slug of module
-- @param policy (string) policy slug
-- @param input (string) input content
function change_policy:approve_change(module_slug, policy, input)
   self.org:call_module(
      self.config.approval_module,
      self.initiator,
      {prompt = "Update " .. policy .. " policy on module " ..
          module_slug .. " with: " .. input .. " ?"},
      function()
         modpol.interactions.message(
            self.initiator,
            "In ".. self.org.name .. " updating " .. policy ..
            " policy on module " .. module_slug .. " with: " .. input)
         self.org.policies[module_slug][policy] = input
         modpol.interactions.org_dashboard(
            self.initiator, self.org.id)
         if self.data.result then self.data.result() end
         self.org:delete_process(self.id)
      end,
      self.id
   )
end

modpol.modules.change_policy = change_policy
