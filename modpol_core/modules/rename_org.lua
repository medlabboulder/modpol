--- Rename org
-- Calls a process on an org to rename it.
-- @module rename_org

local rename_org = {
    name = "Rename this org",
    slug = "rename_org",
    desc = "Renames an org."
}

rename_org.data = {
   result = nil,
   new_name = nil
}

rename_org.config = {
   approval_module = false
}

--- Renames the org after consent is reached
-- @function rename_org:initiate
-- @param result Callback if this module is embedded in other modules
function rename_org:initiate(result)
   modpol.interactions.text_query(
      self.initiator,"New org name: ",
      function(input)
         if input == "" then
            modpol.interactions.message(
               self.initiator,
               "No name entered for child org")
            modpol.interactions.org_dashboard(
               self.initiator, self.org.name)
            self.org:delete_process(self.id)            
            if result then result() end
            return
         elseif modpol.orgs.get_org(input) then
            modpol.interactions.message(
               self.initiator,
               "Org name already in use")
            modpol.interactions.org_dashboard(
               self.initiator, self.org.name)
            self.org:delete_process(self.id)
            if result then result() end
            return
         end
         self.data.new_name = input
         modpol.interactions.message(
               self.initiator,
               "Proposed to change name of org " ..
               self.org.name .. " to " .. input)
         -- initiate consent process
         self:call_module(
            self.config.approval_module,
            self.initiator, 
            {
               prompt = "Change name of org " ..
                  self.org.name .. " to " .. input .. "?"
            },
            function()
               self:complete()
            end
         )
         modpol.interactions.org_dashboard(
            self.initiator, self.org.name)
      end
   )
end

--- Changes the name of the org
-- @funciton rename_org
function rename_org:complete()
   modpol.interactions.message_org(
      self.initiator,
      self.org.name,
      "Changing name of org " .. self.org.name ..
      " to " .. self.data.new_name)
   self.org.name = self.data.new_name
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
end

modpol.modules.rename_org = rename_org
