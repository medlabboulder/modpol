--- Messages all org members
-- @module message_org

local message_org = {
    name = "Message org",
    slug = "message_org",
    desc = "Send a message to all members of the org",
    hide = false;
}

message_org.data = {
}

message_org.config = {
}

--- Allows user to send input message to all org members
-- @function initiate
-- @param result Callback if this module is embedded in other modules
function message_org:initiate(result)
   modpol.interactions.text_query(
      self.initiator, "Message to all org members: ",
      function(input)
         local message = self.initiator.." to org "..
            self.org.name..": "..input
         if input and input ~= "" then
            modpol.interactions.message_org(
               self.initiator, self.org.id, message)
         end
         modpol.interactions.org_dashboard(
            self.initiator, self.org.id)

      end
   )
   if result then result() end
   self.org:delete_process(self.id)
end

modpol.modules.message_org = message_org
