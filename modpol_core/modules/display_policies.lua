--- Display policies
-- @module display_policies

local display_policies = {
    name = "Display policies",
    slug = "display_policies",
    desc = "Presents a detailed list of org policies",
    hide = false;
}

display_policies.data = {
}

display_policies.config = {
}

--- Initiate function
-- @function display_policies:initiate
-- @param result Callback if this module is embedded in other modules
function display_policies:initiate(result)
   local display_table = {}
   for k,v in pairs(self.org.policies) do
      if v then -- check the module is allowed
         local input = modpol.modules[k].name
         table.insert(display_table, input)
         if modpol.modules[k].config then
            for k2,v2 in pairs(modpol.modules[k].config) do
               if self.org.policies[k][k2] then
                  v2 = self.org.policies[k][k2]
               end
               local v2_string = ""
               if not v2 then
                  v2_string = "none"
               elseif type(v2) == "string" then
                  v2_string = v2
               elseif type(v2) == "table"
                  or type(v2) == "number" then
                  v2_string = tostring(v2)
               elseif type(v2) == "boolean" then
                  v2_string = tostring(v2)
               else
                  v2_string = "Could not render"
               end
               input = k2..": "..v2_string
               table.insert(display_table, input)
            end
         end
         table.insert(display_table, "---")
      end
   end
   local output = table.concat(display_table,"\n")
   if #display_table == 0 then
      output = "No modules found"
   end
   modpol.interactions.display(
      self.initiator,
      "Policies in org "..self.org.name,
      output,
      function()
         self.org:delete_process(self.id)
         modpol.interactions.org_dashboard(
            self.initiator, self.org.id)
         if result then result() end
      end
   )
end

modpol.modules.display_policies = display_policies
