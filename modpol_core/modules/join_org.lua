--- Join org
-- Adds initiator to an org
-- @module join_org

local join_org = {
    name = "Join this org",
    slug = "join_org",
    desc = "Allows initiator to join this org"
}

join_org.data = {
   result = nil
}

join_org.config = {
   approval_module = false
}

--- Initiate join org with consent
-- @function join_org:initiate
-- @param result Callback if this module is embedded in other modules
function join_org:initiate(result)
   if self.org:has_member(self.initiator) then
      modpol.interactions.message(
         self.initiator,
         "You are already a member of this org")
      if result then result() end
      self.org:delete_process(self.id)
   else
      self.data.result = result
      self:call_module(
         self.config.approval_module, 
         self.initiator, 
        {
            prompt = "Allow " .. self.initiator .. " to join?"
        },
        function () 
            self:complete()
        end
    )
   end
end

--- Adds member to org, notifies org, and deletes process
-- @function join_org:complete
function join_org:complete()
   self.org:add_member(self.initiator)
   modpol.interactions.message_org(
      self.initiator,self.org.name,
      self.initiator .. " joined org " .. self.org.name)
   if self.data.result then self.data.result() end
   self.org:delete_process(self.id)
end

modpol.modules.join_org = join_org
